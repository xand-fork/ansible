from __future__ import (absolute_import, division, print_function)
from unittest import mock, TestCase
from mock import MagicMock
from ansible_collections.tpfsio.ansible.plugins.modules import create_jwt
__metaclass__ = type

ANSIBLE_MODULE_NAME = 'ansible_collections.tpfsio.ansible.plugins.modules.create_jwt.AnsibleModule'
STDOUT = 'stdout'
STDERR = 'stderr'
SUCCESS_RETURN_CODE = 0


class CreateJWTModuleTests(TestCase):

    def build_fake_module_with_secret(self,
                                      run_command_return_value=(SUCCESS_RETURN_CODE, STDOUT, STDERR),
                                      bin_path='/some/bin/folder',
                                      secret='whatapassword'):
        params = {
            'claims': ["iss=xand", "sub=client"],
            'secret': secret,
            'jwks_path': None
        }

        return self.build_fake_module(params, run_command_return_value, bin_path)

    def build_fake_module_with_jwks_path(self,
                                         run_command_return_value=(SUCCESS_RETURN_CODE, STDOUT, STDERR),
                                         bin_path='/some/bin/folder',
                                         jwks_path='/tmp/jwks'):
        params = {
            'claims': ["iss=xand", "sub=client"],
            'secret': None,
            'jwks_path': jwks_path
        }

        return self.build_fake_module(params, run_command_return_value, bin_path)

    def build_fake_module(self,
                          params,
                          run_command_return_value=(SUCCESS_RETURN_CODE, STDOUT, STDERR),
                          bin_path='/some/bin/folder'):
        fake_module = MagicMock()
        fake_module.run_command = MagicMock(return_value=run_command_return_value)
        fake_module.get_bin_path.return_value = bin_path
        fake_module.params = params
        return fake_module

    def test_runs_jwtcli_with_secret(self):
        fake_module = self.build_fake_module_with_secret(bin_path='/fake/bin/jwtcli')
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            create_jwt.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/jwtcli', 'jwt', '--secret', "whatapassword", "--claims", "iss=xand", "--claims", "sub=client"],
                check_rc=True)

    def test_runs_jwtcli_with_jwks_path(self):
        fake_module = self.build_fake_module_with_jwks_path(bin_path='/fake/bin/jwtcli', jwks_path="/tmp/jwks2")
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            create_jwt.main()
            fake_module.run_command.assert_called_with(
                ['/fake/bin/jwtcli', 'jwt', '--jwks', "/tmp/jwks2", "--claims", "iss=xand", "--claims", "sub=client"],
                check_rc=True)

    def test_exit_json_contains_return_code_and_stderr_and_jwt(self):
        error_return_code = 5555
        fake_module = self.build_fake_module_with_secret(run_command_return_value=(error_return_code, STDOUT, STDERR))
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            create_jwt.main()
            fake_module.exit_json.assert_called_with(rc=5555, jwt=STDOUT, stderr=STDERR, changed=False)

    def test_jwtcli_is_required_to_exist(self):
        fake_module = self.build_fake_module_with_secret(bin_path=None)
        with mock.patch(ANSIBLE_MODULE_NAME, return_value=fake_module):
            create_jwt.main()
            fake_module.get_bin_path.assert_called_with("jwtcli", required=True)
