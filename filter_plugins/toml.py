from __future__ import (absolute_import, division, print_function)
import toml
__metaclass__ = type


def from_toml(str):
    return toml.loads(str)


def to_toml(object):
    return toml.dumps(object)


class FilterModule(object):
    def filters(self):
        return {
            'to_toml': to_toml,
            'from_toml': from_toml
        }
