#!/usr/bin/env bash

# Exit if error occurs
set -e

# Start with clean folder
rm -rf ansible_collections

# Move the collection into the required path to make ansible-test work. See here for more deets:
# https://github.com/ansible/ansible/issues/60215
mkdir -p ansible_collections/tpfsio/ansible
find . -maxdepth 1 ! -regex '.*/ansible_collections' ! -regex '.' ! -regex '.*/.git' ! -regex '.*/.gitlab-ci' \
  -exec cp -r '{}' ansible_collections/tpfsio/ansible \;
cd ansible_collections/tpfsio/ansible

# Copies all our python modules into plug-ins folder.
# The sanity tests and unit tests expect all modules to be in <collection_root>/plugins/modules
mkdir -p plugins/modules
mv roles/*/library/*.py plugins/modules

# collection must be in a GIT repo, and not under a .git ignored subfolder. WHAT?!
# https://github.com/ansible/ansible/issues/63032
git init

# Run tests
export PYTHON_VERSION=$(python3 --version | grep Python | sed -e "s/.*Python \(.*\)\.[0-9]*/\1/")
ansible-test sanity --python $PYTHON_VERSION
ansible-test units --python $PYTHON_VERSION
