#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import argparse
import subprocess
import os
from data_types import PartnerType
from sanitization import sanitize_ansible_directory
from shutil import copytree
from typing import Any, Dict
from vars import prepare_vars_folder_for_partner
import zipfile

PARTNER_TYPE = "partner_type"
ANSIBLE_FOLDER_PATH = "ansible_folder_path"
OUTPUT_DIR = "output_dir"
ZIP_FILE_PATH = "zip_file_path"


def main():
    """The main function which will take care of all the steps to be able to create a zip for validator artifacts.
    """
    args = _get_args()
    ansible_path = args[ANSIBLE_FOLDER_PATH]
    output_dir = args[OUTPUT_DIR]
    partner_type = args[PARTNER_TYPE]
    zip_file_path = args[ZIP_FILE_PATH]

    print(f"Copying files to output directory. '{ansible_path}' to '{output_dir}'")
    copytree(ansible_path, output_dir)

    sanitize_ansible_directory(output_dir, partner_type)
    prepare_vars_folder_for_partner(output_dir, partner_type)
    _zip_output(output_dir, zip_file_path)


def _get_args() -> Dict[str, Any]:
    """Parses the arguments passed to the python script. Argparse will also display help messages to help the user
    pass the right arguments.
    """
    parser = argparse.ArgumentParser(description="Make an ansible validator zip file.")

    parser.add_argument("partner_type",
                        type=PartnerType.from_string,
                        choices=list(PartnerType),
                        help="The type of partner you will be creating a zip for.")

    parser.add_argument("ansible_folder_path",
                        type=str,
                        metavar="ANSIBLE_FOLDER_PATH",
                        help="The path to the folder containing all of transparent system's ansible resources.")

    parser.add_argument("output_dir",
                        type=str,
                        metavar="OUTPUT_DIR",
                        help="The output folder where the deployment artifacts will land.")

    parser.add_argument("zip_file_path",
                        type=str,
                        metavar="ZIP_FILE_PATH",
                        help="The output zip file path where the deployment artifacts will be zipped up in to.")

    args = parser.parse_args()
    return {
        PARTNER_TYPE: args.partner_type,
        ANSIBLE_FOLDER_PATH: args.ansible_folder_path,
        OUTPUT_DIR: args.output_dir,
        ZIP_FILE_PATH: args.zip_file_path
    }


def _zip_output(output_dir: str, zip_file_path: str):
    zipf = zipfile.ZipFile(zip_file_path, mode='w')
    output_dir_length = len(output_dir)

    for root, _unused_, files in os.walk(output_dir):
        for file in files:
            file_path = os.path.join(root, file)
            # Strip out the output dir path from the file path
            zipf.write(file_path, file_path[output_dir_length:])
    zipf.close()


if __name__ == "__main__":
    main()
