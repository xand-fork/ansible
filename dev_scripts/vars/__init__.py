#!/usr/bin/env python
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from data_types import PartnerType
import glob
import os
from os.path import basename, exists, join
from shutil import move

files_specific_to_partner_type = {
    "trust.yml.template": [PartnerType.Trust],
    "bank_credentials.yml.template": [PartnerType.Member, PartnerType.Trust]
}


def prepare_vars_folder_for_partner(directory_path: str, partner_type: PartnerType):
    vars_path = join(directory_path, "vars")

    _manage_template_files(vars_path, partner_type)
    _remove_other_files(vars_path)


def _manage_template_files(vars_path: str, partner_type: PartnerType):
    vars_template_glob = join(vars_path, "*.yml.template")
    vars_templates = glob.glob(vars_template_glob)

    for vars_template in vars_templates:
        filename = basename(vars_template)

        # Remove the .template to get the destination file path.
        template_length = -1 * len(".template")
        yml_file_path = vars_template[:template_length]

        # Remove corresponding file if it exists before overwriting it.
        if exists(yml_file_path):
            os.remove(yml_file_path)

        # Check if the filename of vars template exists in the dictory to check
        # against partner type and if so then check if we're not with the corresponding
        # partner type. (We'll remove the file if not otherwise act like it's a normal template file.)
        if filename in files_specific_to_partner_type and \
           partner_type not in files_specific_to_partner_type[filename]:

            os.remove(vars_template)
        else:
            move(vars_template, yml_file_path)


def _remove_other_files(vars_path: str):
    non_yml_files = [file for file in os.listdir(vars_path) if not file.endswith(".yml")]

    for file in non_yml_files:
        os.remove(join(vars_path, file))
