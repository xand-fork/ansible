#!/usr/bin/env bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

function helptext () {
    local text=$(cat <<EOF
    Set up Azure service principal for use with Ansible in specified subscription.

EOF
	  )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

#set script dependencies here...
declare -a DEPENDENCY=("az" "jq")

#Check if each depedency is installed...
for val in ${DEPENDENCY[@]}; do
  if ! command -v $DEPENDENCY &> /dev/null; then
    error "$DEPENDENCY is not installed. Please install $DEPENDENCY and re-run this script."
  fi
done

#finding where this script exists
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

#checking we can find necessary files
if [[ ! -d $DIR/../vars/ ]]; then
  error "cannot find vars folder, aborting"
fi

if [[ ! -f $DIR/../vars/aks.yml && -f $DIR/../vars/aks.yml.template ]]; then
  cp $DIR/../vars/aks.yml.template $DIR/../vars/aks.yml
fi

#Check if logged in to azure...
if [[ $(az account list | jq length) -eq 0 ]]; then
    info "Logging into Azure ..."
    az login
fi

#use jq to take output from az account list and put it in an associative array
declare -A ACCOUNTS="($(
  az account list \
  | jq -r '.[]  | "[" + .id + "]=\"" +.name + "\""' \
  ))"

#Here we're prompting the user which subscription to use.
printf "\nSelect the subscription you would like to create an owner role service principal in:\n"
PS3="Choose an account: "
select ACTIVE_ACCOUNT in "${ACCOUNTS[@]}" "Exit"; do

  if [[ $REPLY > 0 ]] && [[ $REPLY -le ${#ACCOUNTS[@]} ]]; then
    #Iterating through the associative array to find the ID, which is the key
    for key in "${!ACCOUNTS[@]}"; do
      if [[ ${ACCOUNTS[$key]} == "$ACTIVE_ACCOUNT" ]]; then
        ACTIVE_ACCOUNT_ID=$key
      fi
    done
    break
  elif [[ $ACTIVE_ACCOUNT == "Exit" ]]; then
    error "Goodbye"
  else
    info "You selected an invalid option\n"
  fi

done

#Now we set the subscription to the selected account
az account set --subscription="$ACTIVE_ACCOUNT_ID"
info "\n$ACTIVE_ACCOUNT - $ACTIVE_ACCOUNT_ID has been set as the selected account\n"

#Create the service principal, put json output in a variable
info "Securely store the following credentials. These credentials have owner role permissions to alter resources in the selected Azure subscription.\n"
AZURE_CREDENTIALS=$(az ad sp create-for-rbac --role="Owner" --scopes="/subscriptions/$ACTIVE_ACCOUNT_ID" --name http://xand-$(date "+%Y-%m-%d-%H-%M-%S"))
info "\n$AZURE_CREDENTIALS\n"
APP_ID=$(jq -r '.appId' <<< "$AZURE_CREDENTIALS")
TENANT_ID=$(jq -r '.tenant' <<< "$AZURE_CREDENTIALS")
TENANT_SECRET=$(jq -r '.password' <<< "$AZURE_CREDENTIALS")

#If the aks.yml file does exist, we'll just add the information into it
sed -i"" -e "s/subscription_id:.*/subscription_id: \"$ACTIVE_ACCOUNT_ID\"/" \
  -e "s/client_id:.*/client_id: \"$APP_ID\"/" \
  -e "s/tenant_id:.*/tenant_id: \"$TENANT_ID\"/" \
  -e "s/client_secret:.*/client_secret: \"$TENANT_SECRET\"/" \
  $DIR/../vars/aks.yml
info "The aks.yml was updated to include the Service Principal credentials that were just generated.\n"
