#!/usr/bin/env bash

set -o errexit # abort on nonzero exit status
set -o nounset # abort on unbound variable
set -o pipefail # abort if any element of a pipeline fails

function helptext () {
    local text=$(cat <<EOF
    Set up gcloud service account for use with Ansible
EOF
	  )
    echo "$text"
}

function error () {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

function info () {
    local green="\033[32m"
    local normal="\033[00m"
    echo -e "${green}$1${normal}"
}

#set script dependencies here...
declare -a DEPENDENCY=("gcloud" "jq")

#Check if each depedency is installed...
for val in ${DEPENDENCY[@]}; do
  if ! command -v "$DEPENDENCY" &> /dev/null; then
    error "$DEPENDENCY is not installed. Please install $DEPENDENCY and re-run this script."
  fi
done

#finding where this script exists
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

#checking we can find necessary files
if [[ ! -d $DIR/../vars/ ]]; then
  error "cannot find vars folder, aborting"
fi

if [[ ! -f $DIR/../vars/gke.yml && -f $DIR/../vars/gke.yml.template ]]; then
  cp $DIR/../vars/gke.yml.template $DIR/../vars/gke.yml
fi

#logging in to gcloud
if gcloud config list account | grep -q "unset"; then
    info "Logging into gcloud ..."
    gcloud auth login
fi

#use jq to take output from gcloud project list and put it in an associative array
declare -A PROJECTS="($(
  gcloud projects list --format="json" \
  | jq -r '.[]  | "[" + .projectId + "]=\"" +.name + "\""' \
  ))"

#Here we're prompting the user which project to use.
printf "\nSelect the project you would like to create a service account in:\n"
PS3="Choose a project: "
select ACTIVE_PROJECT in "${PROJECTS[@]}" "Exit"; do

  if [[ $REPLY > 0 ]] && [[ $REPLY -le ${#PROJECTS[@]} ]]; then
    #Iterating through the associative array to find the ID, which is the key
    for key in "${!PROJECTS[@]}"; do
      if [[ ${PROJECTS[$key]} == "$ACTIVE_PROJECT" ]]; then
        ACTIVE_PROJECT_ID=$key
      fi
    done
    break
  elif [[ $ACTIVE_PROJECT == "Exit" ]]; then
    error "Goodbye"
  else
    info "You selected an invalid option\n"
  fi

done

#Now we set the subscription to the selected account
gcloud config set project $ACTIVE_PROJECT_ID
info "\n$ACTIVE_PROJECT - $ACTIVE_PROJECT_ID has been set as the selected account\n"

#Create the service user, assign permissions, and generate keys
XAND_SERVICE_USER_NAME="xand-$(date "+%Y-%m-%d-%H-%M-%S")"
gcloud iam service-accounts create $XAND_SERVICE_USER_NAME --display-name="$XAND_SERVICE_USER_NAME" > /dev/null
gcloud projects add-iam-policy-binding $ACTIVE_PROJECT_ID --member="serviceAccount:$XAND_SERVICE_USER_NAME@$ACTIVE_PROJECT_ID.iam.gserviceaccount.com" --role="roles/owner" > /dev/null
gcloud iam service-accounts keys create "$DIR/../vars/$XAND_SERVICE_USER_NAME.json" \
    --iam-account=$XAND_SERVICE_USER_NAME@$ACTIVE_PROJECT_ID.iam.gserviceaccount.com > /dev/null
info "\nA service user with owner role was created in $ACTIVE_PROJECT_ID called $XAND_SERVICE_USER_NAME.\n\nA json file called $XAND_SERVICE_USER_NAME.json was created in the Ansible vars folder. These credentials have owner role permissions to alter resources in the selected gcloud and should be stored securely.\n"

#Creating a path to the Ansible vars folder, and an escaped version of that path
SERVICE_ACCOUNT_SRC="$(echo ${DIR%/scripts})/vars/$XAND_SERVICE_USER_NAME.json"
ESCAPED_SERVICE_ACCOUNT_SRC="$(echo $SERVICE_ACCOUNT_SRC | sed 's./.\\/.g')"

sed -i"" -e "s/project_id: \"\"/project_id: \"$ACTIVE_PROJECT_ID\"/" \
  -e "s/service_account_src:.*/service_account_src: \"$ESCAPED_SERVICE_ACCOUNT_SRC\"/" \
  $DIR/../vars/gke.yml
info "The gke.yml was updated to include the path to the credential json file that was just generated. If you move the credential json file, you will also need to update the service_account_src path.\n"
