#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: generate_libp2p

short_description: Generates a libp2p key for a substrate node.

version_added: "1.0.0"

description:
    - "Uses xkeygen to generate the libp2p key."

options:
    facts_dir:
        description:
            - The directory to read and write facts to.
        required: true
        type: path
    xand_dir:
        description:
            - The location to output the keys generated from this task.
        required: true
        type: path

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
# Pass in a message and have changed true
- name: Generate Xand Node Libp2p Key
  generate_validator_keys:
    facts_dir: "{{ facts_dir }}"
    xand_dir: ~/xand/non-consensus-node
'''

RETURN = '''
libp2p_keys:
    description: Public/Private keypair for libp2p
    type: complex
    returned: always
    contains:
        libp2p_peering_id:
            description: The public key base58 encoded of the libp2p peering key.
            type: str
            returned: always
        libp2p_node_key:
            description: The private key generated as a node key for libp2p peering.
            type: str
            returned: always
'''

BASE58_CHARACTER_CLASS = "[1-9A-HJ-NP-Za-km-z]"

import json
import os
import re
from ansible.module_utils.basic import AnsibleModule  # type: ignore
__metaclass__ = type


def _xkeygen_generate(module, xkeygen) -> dict:
    (return_code, libp2p_output, error) = module.run_command(
        [xkeygen, "generate", "validator-libp2p"],
        check_rc=True,
        cwd=module.params['xand_dir']
    )

    # The new p2p keys are 52 characters: 12D3KooWBMKEsrWZocuuSADQnnwjZAsxgZNLu4fhsRNcAhBf6Hr4
    match = re.search(f'{BASE58_CHARACTER_CLASS}{{52}}', libp2p_output)
    if match:
        peering_id = match.group(0)
    else:
        raise KeyError('Not able to find the libp2p peering id in the output of xkeygen.')

    libp2p_keys_env_path = os.path.join(module.params['xand_dir'], "validator-lp2p-key.env")
    with open(libp2p_keys_env_path, 'r') as libp2p_keys_env_file:
        node_key_content = libp2p_keys_env_file.read()

    match = re.search('^NODE_KEY=([^\n]+)', node_key_content)
    if match:
        node_key = match.group(1)
    else:
        raise KeyError('Not able to find the libp2p node key from the validator-lp2p-key.env.')

    return dict(
        libp2p_peering_id=peering_id,
        libp2p_node_key=node_key
    )


def _completed_key_generation(libp2p_keys):
    return 'libp2p_peering_id' in libp2p_keys and \
        'libp2p_node_key' in libp2p_keys


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        facts_dir=dict(type='path', required=True),
        xand_dir=dict(type='path', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        libp2p_keys=dict(
            libp2p_peering_id=''
        )
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Automatically calls module.fail_json if the binary is not found.
    xkeygen = module.get_bin_path("xkeygen", required=True)

    # Using DataLoader as it will be helpful later if used with ansible
    # vault.
    libp2p_keys_path = os.path.join(module.params['facts_dir'], "libp2p_keys.fact")

    # Loads the libp2p_keys fact in case this has run in the past.
    if os.path.exists(libp2p_keys_path):
        with open(libp2p_keys_path, 'r') as libp2p_keys_file:
            result['libp2p_keys'] = json.load(libp2p_keys_file)

        # The keys already exist so no need to generate new ones just pass the results back
        if _completed_key_generation(result['libp2p_keys']):
            module.exit_json(**result)

    # if the user is working with this module in only check mode we do not
    # want to make any changes to the environment, just return the current
    # state with no modifications
    if module.check_mode:
        module.exit_json(**result)

    try:
        result['libp2p_keys'] = \
            _xkeygen_generate(module, xkeygen)

    except Exception as e:
        module.fail_json(e.message, **result)

    with open(libp2p_keys_path, 'w') as libp2p_keys_file:
        libp2p_keys_file.write(module.jsonify(result['libp2p_keys']))

    # We will be creating keys and so changed will be true.
    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
