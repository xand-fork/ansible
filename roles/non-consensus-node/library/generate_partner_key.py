#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: generate_partner_key

short_description: Generates a partner key for a substrate node.

version_added: "1.0.0"

description:
    - "Uses xkeygen to generate the partner key."

options:
    facts_dir:
        description:
            - The directory to read and write facts to.
        required: true
        type: path
    xand_dir:
        description:
            - The location to output the keys generated from this task.
        required: true
        type: path
    partner_type:
        description:
            - The type of partner to generate a key for.
        required: true
        type: str

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
- name: Generate Member Key
  generate_partner_key:
    facts_dir: /root/xand/ansible/facts.d/
    xand_dir: /root/xand/non-consensus-node
    partner_type: Member
'''

RETURN = '''
partner_key:
    description: Public Address for a Partner Key
    type: complex
    returned: success
    contains:
        address:
            description: The public address of the partner key.
            type: str
            returned: success
'''

BASE58_CHARACTER_CLASS = "[1-9A-HJ-NP-Za-km-z]"

import json
import os
import re
from ansible.module_utils.basic import AnsibleModule  # type: ignore
__metaclass__ = type


PARTNER_TYPE_TO_KEY = {
    "member": "wallet",
    "trust": "trust",
    "limited-agent": "limited-agent"
}


def _lookup_partner_key(partner_type) -> str:
    # Case-Insensitive (ci) by making it lowercase in the searches.
    ci_partner_type = partner_type.lower()

    if ci_partner_type not in PARTNER_TYPE_TO_KEY:
        raise Exception(f"The partner type '{partner_type}' is not a known partner type in generate_partner_key.")

    return PARTNER_TYPE_TO_KEY[ci_partner_type]


def _xkeygen_generate(module, xkeygen, partner_key_type) -> str:
    (return_code, key_output, error) = module.run_command(
        [xkeygen, "generate", partner_key_type],
        check_rc=True,
        cwd=module.params['xand_dir']
    )

    match = re.search(f'{BASE58_CHARACTER_CLASS}{{48}}', key_output)
    if match:
        return match.group(0)
    else:
        raise KeyError(f'Not able to find the {partner_key_type} in the output of xkeygen.')


def _completed_key_generation(partner_key):
    return 'address' in partner_key


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        facts_dir=dict(type='path', required=True),
        xand_dir=dict(type='path', required=True),
        partner_type=dict(type='str', required=True)
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        partner_key=dict(
            address=''
        )
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    partner_type = module.params["partner_type"]

    # Automatically calls module.fail_json if the binary is not found.
    xkeygen = module.get_bin_path("xkeygen", required=True)

    # Using DataLoader as it will be helpful later if used with ansible
    # vault.
    partner_key_path = os.path.join(module.params['facts_dir'], "partner_key.fact")

    # Loads the partner_key fact in case this has run in the past.
    if os.path.exists(partner_key_path):
        with open(partner_key_path, 'r') as partner_key_file:
            result['partner_key'] = json.load(partner_key_file)

        # The keys already exist so no need to generate new ones just pass the results back
        if _completed_key_generation(result['partner_key']):
            module.exit_json(**result)

    try:
        partner_key_type = _lookup_partner_key(partner_type)

        # if the user is working with this module in only check mode we do not
        # want to make any changes to the environment, just return the current
        # state with no modifications
        if module.check_mode:
            module.exit_json(**result)

        result['partner_key']['address'] = \
            _xkeygen_generate(module, xkeygen, partner_key_type)

    except Exception as e:
        module.fail_json(e.message, **result)

    with open(partner_key_path, 'w') as partner_key_file:
        partner_key_file.write(module.jsonify(result['partner_key']))

    # We will be creating keys and so changed will be true.
    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
