- name: Generate Key
  generate_partner_key:
    facts_dir: "{{ facts_dir }}"
    xand_dir: "{{ xand_dir }}"
    partner_type: "{{ partner_type }}"
  register: partner_key

- set_fact:
    partner_is_member_or_trust: "{{ partner_type | regex_search('trust|member', ignorecase=True) | default('') }}"

- name: Generate Encryption Key
  generate_encryption_key:
    facts_dir: "{{ facts_dir }}"
    xand_dir: "{{ xand_dir }}"
  register: encryption_key
  when: partner_is_member_or_trust | length > 0

- name: Get the kustomization definition
  slurp:
    src: "{{ (xand_dir, 'kustomization.yaml') | path_join }}"
  register: xand_kustomization_definition

- set_fact:
    xand_kustomization_content: "{{ xand_kustomization_definition['content'] | b64decode | from_yaml }}"

- set_fact:
    kustomization_address: "{{ xand_kustomization_content | json_query(xand_partner_key_query) }}"
  # jmespath query that looks for the xand-keys secret
  # and checks for a file that contains the address
  # If already there it will skip this step.
  vars:
    xand_partner_key_query: "secretGenerator[?name =='validator-xand-keys'].files[] | [?contains(@, '{{ partner_key.partner_key.address }}')] | [0]"

- set_fact:
    kustomization_encryption_key: "{{ xand_kustomization_content | json_query(xand_encryption_key_query) }}"
  # jmespath query that looks for the xand-keys secret
  # and checks for a file that contains the address
  # If already there it will skip this step.
  vars:
    xand_encryption_key_query: "secretGenerator[?name =='validator-xand-keys'].files[] | [?contains(@, '{{ encryption_key.encryption_key.public_key }}')] | [0]"
  when: partner_is_member_or_trust | length > 0

- name: Kustomize add Partner Type Address to Xand Keys
  kustomize_edit:
    kustomization_dir: "{{ xand_dir }}"
    edit_operation: add
    edit_type: secret
    edit_value: "validator-xand-keys --from-file kp-{{ partner_key.partner_key.address }}"
  when: "kustomization_address != 'kp-' ~ partner_key.partner_key.address"

- name: Kustomize add Encryption Public Key to Xand Keys
  kustomize_edit:
    kustomization_dir: "{{ xand_dir }}"
    edit_operation: add
    edit_type: secret
    edit_value: "validator-xand-keys --from-file kp-{{ encryption_key.encryption_key.public_key }}"
  when: "partner_is_member_or_trust | length > 0 and kustomization_encryption_key != 'kp-' ~ encryption_key.encryption_key.public_key"

- name: Kustomize Set Namespace
  kustomize_edit:
    kustomization_dir: "{{ xand_dir }}"
    edit_operation: set
    edit_type: namespace
    edit_value: "{{ namespace }}"

- name: Kustomize Build NonConsensus Node
  kustomize_build:
    kustomization_dir: "{{ xand_dir }}"
  register: kustomize_build_xand
  when: should_deploy_xand_node

- name: Kubectl apply Xand NonConsensus Node
  community.kubernetes.k8s:
    apply: yes
    resource_definition: "{{ kustomize_build_xand.stdout }}"
  environment:
    KUBECONFIG: "{{ remote_kubeconfig_path }}"
  when: ((dry_run is not defined) or (dry_run == false)) and kustomize_build_xand.stdout is defined

- name: Verify Validator Deployment
  vars:
    validator_deployment_name: validator
  import_role:
    name: xand-node
    tasks_from: verify_validator_deployed.yml
  when: ((dry_run is not defined) or (dry_run == false)) and kustomize_build_xand.stdout is defined
