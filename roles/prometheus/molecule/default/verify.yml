---
- name: Verify
  hosts: molecule-toolchain
  gather_facts: false
  tasks:
  - name: Import Variables via Tasks
    include_tasks:
      file: ../../../../import_vars.yml

  - name: Get the Prometheus Ingress Object
    community.kubernetes.k8s_info:
      api_version: networking.k8s.io/v1
      kind: Ingress
      namespace: monitoring
      name: prometheus
    environment:
      KUBECONFIG: "{{ remote_kubeconfig_path }}"
    register: prometheus_ingress

  - name: Check that the Ingress has been specified.
    assert:
      that: prometheus_ingress is defined

  - set_fact:
      prometheus_ingress_spec: "{{ prometheus_ingress.resources[0].spec }}"
      prometheus_domain_secret_name: "{{ prometheus_domain_name | regex_replace('\\.', '-') }}"

  - name: Check that the Xand API Ingress has domain and secrets.
    assert:
      that:
        - prometheus_ingress_spec.rules[0].host == "{{ prometheus_domain_name }}"
        - prometheus_ingress_spec.tls[0].hosts[0] == "{{ prometheus_domain_name }}"
        - prometheus_ingress_spec.tls[0].secretName == "{{ prometheus_domain_secret_name }}"

  - name: Retrieve the Prometheus Deployment until ready
    community.kubernetes.k8s_info:
      api_version: apps/v1
      kind: Deployment
      namespace: monitoring
      name: prometheus
    environment:
      KUBECONFIG: "{{ remote_kubeconfig_path }}"
    register: prometheus_deployment
    until: prometheus_deployment.resources[0].status.readyReplicas is defined and prometheus_deployment.resources[0].status.readyReplicas == prometheus_deployment.resources[0].spec.replicas
    retries: 30
    delay: 10

  - name: Wait for additional Check
    wait_for:
      timeout: 10

  - name: Retrieve Prometheus Deployment
    community.kubernetes.k8s_info:
      api_version: apps/v1
      kind: Deployment
      namespace: monitoring
      name: prometheus
    environment:
      KUBECONFIG: "{{ remote_kubeconfig_path }}"
    register: prometheus_deployment

  - name: Check that Prometheus is still available
    assert:
      that: prometheus_deployment.resources[0].status.readyReplicas == prometheus_deployment.resources[0].spec.replicas
