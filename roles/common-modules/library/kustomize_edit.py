#!/usr/bin/python
from __future__ import (absolute_import, division, print_function)

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'Engineering Transparent Financial Systems'
}

DOCUMENTATION = '''
---
module: kustomize_edit

short_description: Performs kustomize edit commands to run during the deployment.

version_added: "1.0.0"

description:
    - "Uses kustomize to perform alterations to the kustomization.yaml file."

options:
    kustomization_dir:
        description:
            - The directory where the kustomization.yaml file exists that will be edited.
        type: path
        required: true
    edit_operation:
        description:
            - The type of edit operation of "add", "fix", "remove", or "set".
        type: str
        required: true
    edit_type:
        description:
            - The type of object being edited. This is optional since the options that might be available here are dependent on the type of the edit operation.
        type: str
        required: false
    edit_value:
        description:
            - The value(s) to set on the edit via an array. Format of this value and what's expected vary based on type of operation and type of edit.
        type: str
        required: false

author:
    - Transparent Financial Systems Engineering (engineering@tfps.io)
'''

EXAMPLES = '''
# Specifying the kustomize edit command that sets the validator image to use in the validator deployment
- name: Set Validator Image
  kustomize_edit:
    kustomization_dir: /path/to/my/kustomize
    edit_operation: set
    edit_type: image
    edit_value: gcr.io/xand-dev/validator={{ validator_image }}

# Specifying the kustomize edit command that sets the validator image to use in the validator deployment
- name: Set Validator Image
  kustomize_edit:
    kustomization_dir: /path/to/my/kustomize
    edit_operation: set
    edit_type: namesuffix
    edit_value: "-something-wicked-this-way-comes"
'''

RETURN = '''
kustomize_edit:
    description: Result of kustomize edit process
    type: complex
    returned: always
    contains:
        kustomize_path:
            description: Path to kustomization file in case it's useful.
            type: str
            returned: always
'''

import json
import os
import re
from ansible.module_utils.basic import AnsibleModule  # type: ignore
from typing import Optional, Tuple, Union
__metaclass__ = type


EDIT_OPERATIONS = {"add", "fix", "remove", "set"}
EDITABLE_TYPES = {
    "add": {"annotation", "base", "component", "configmap", "label", "patch", "resource", "secret"},
    "set": {"image", "nameprefix", "namespace", "namesuffix", "replicas"},
    "remove": {"annotation", "label", "patch", "resource"}
}


def _validate_operation(edit_operation: str) -> None:
    if edit_operation not in EDIT_OPERATIONS:
        raise Exception(f"The edit_operation '{edit_operation}' is not available in kustomize edit.")


def _validate_type(edit_operation: str, edit_type: Optional[str]) -> None:
    if edit_type and edit_operation not in EDITABLE_TYPES:
        raise Exception(f"The edit_operation '{edit_operation}' does not have types and one was supplied '{edit_type}'.")
    elif not edit_type and edit_operation in EDITABLE_TYPES:
        raise Exception(f"The edit_operation '{edit_operation}' has types and edit_type was not supplied '{edit_type}'.")


def _validate_value(edit_operation: str, edit_type: Optional[str], edit_value: Optional[str]) -> None:
    if edit_type and not edit_value:
        raise Exception(f"The edit_type '{edit_type}' was specified but it's expected that a value be included with it '{edit_value}'.")

    if edit_type and edit_operation in EDITABLE_TYPES:
        types_available = EDITABLE_TYPES[edit_operation]
        if edit_type not in types_available:
            raise Exception(f"The type '{edit_type}' is not a known type for the edit_operation '{edit_operation}'.")


def _validate_kustomize_parameters(
        edit_operation: str,
        edit_type: Optional[str],
        edit_value: Optional[str]) -> None:

    _validate_operation(edit_operation)
    _validate_type(edit_operation, edit_type)
    _validate_value(edit_operation, edit_type, edit_value)


def _run_kustomize_edit(
        module: AnsibleModule,
        kustomization_dir: str,
        edit_operation: str,
        edit_type: Optional[str],
        edit_value: Optional[str]) -> Tuple[int, str, str]:

    # Automatically calls module.fail_json if the binary is not found.
    kustomize = module.get_bin_path("kustomize", required=True)
    args = [kustomize, "edit", edit_operation]

    if edit_type:
        args.append(edit_type)

    if edit_value:
        values = edit_value.split()
        if len(values) > 0:
            # Check if the value starts with -some-value then add a -- to
            # make it -- -some-value
            match = re.search("^-[^-]", values[0])
            if match:
                args.append("--")
        args.extend(values)

    return module.run_command(
        args,
        check_rc=True,
        cwd=module.params['kustomization_dir']
    )


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        kustomization_dir=dict(type='path', required=True),
        edit_operation=dict(type='str', required=True),
        edit_type=dict(type='str', required=False),
        edit_value=dict(type='str', required=False)
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=False
    )

    kustomization_dir = module.params["kustomization_dir"]
    edit_operation = module.params["edit_operation"]
    edit_type = module.params["edit_type"] if not module.params["edit_type"].isspace() else None
    edit_value = module.params["edit_value"] if not module.params["edit_value"].isspace() else None

    kustomize_path = os.path.join(kustomization_dir, "kustomization.yaml")

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        kustomize_path=kustomize_path
    )

    try:
        _validate_kustomize_parameters(edit_operation, edit_type, edit_value)

        (result["rc"], result["stdout"], result["stderr"]) = _run_kustomize_edit(
            module,
            kustomization_dir,
            edit_operation,
            edit_type,
            edit_value
        )

    except Exception as e:
        module.fail_json(str(e), **result)

    # We will be modifying kustomize and so changed will be true.
    result['changed'] = True

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
