#!/usr/bin/env python
# This script locates runs all the molecule tests, including destroy at the end.
# It is intended to be run by humans or automation FROM THE ROOT FOLDER of the repository.

import os
import sys
import subprocess

ERROR_EXIT_CODE = 1
SUCCESS_EXIT_CODE = 0
MOLECULE_COMMAND_ORDER = ["create", "converge", "verify", "converge", "verify", "cleanup"]


# Represents the result of a subprocess that executed a molecule command. The reveals the exit code and
# prints enough info to narrow down which test failed.
class MoleculeResult:
    def __init__(self, role, subprocess_result):
        self.role = role
        self.subprocess_result = subprocess_result

    def __repr__(self):
        return f'<Molecule Result: role={self.role}, subprocess_result={self.subprocess_result}>'

    def returncode(self):
        return self.subprocess_result.returncode


# This is a molecule scenario. In a role, each sub-folder of the 'molecule' folder is a scenario. The 'test'
# member function executes many molecule commands in order according to the sequence [create, converge, verify, etc...]
# It breaks if any command returns failure, and returns the failure result.
class Scenario:
    def __init__(self, role, scenario_name):
        self.role = role
        self.scenario_name = scenario_name

    def __repr__(self):
        return f"<Scenario {self.role.name()}:{self.scenario_name}>"

    def role_name(self):
        return self.role.name()

    def execute(self, command):
        return subprocess.run([
            "molecule", command,
            "--scenario-name", self.scenario_name],
            cwd=self.role.abs_path())

    def test(self):
        molecule_results = []
        for command in MOLECULE_COMMAND_ORDER:
            result = self.execute(command)
            molecule_results.append(MoleculeResult(self.role_name(), result))
            if result.returncode != SUCCESS_EXIT_CODE:
                break

        return molecule_results


# This is a molecule role which means it is an ansible role with a 'molecule' sub-folder
class Role:
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return f"<Role '{self.name()}'>"

    def abs_path(self):
        return os.path.join(os.getcwd(), "roles", self.name())

    def name(self):
        return self.path.name

    def scenarios(self):
        scenarios_path = os.path.join(self.path, 'molecule')
        return list(Scenario(self, scenario_dir.name)
                    for scenario_dir
                    in os.scandir(scenarios_path)
                    if scenario_dir.is_dir())


# Given role folder path, returns true if it contains a molecule sub-folder, indicating it is a molecule role.
def contains_molecule_subdir(role_directory):
    return 'molecule' in (
        subdir.name
        for subdir
        in os.scandir(role_directory))


# Gets a role instance for every molecule role
def roles(basePath):
    return list(
        Role(role_dir)
        for role_dir
        in os.scandir(basePath)
        if role_dir.is_dir() and contains_molecule_subdir(role_dir))


# takes a list of lists and flattens into a list.
def flatten(list_of_lists):
    return [item for sublist in list_of_lists for item in sublist]


# Given a collection of failed results, prints some error text and sets an error exit code.
def show_failed_scenarios(failed_results):
    print("Found one or more failures!")
    for result in failed_results:
        print(f"Failed scenario!: {result}")


# Runs the molecule destroy process to clean-up the k8s cluster and vpc
def destroy_result():
    print("Running molecule destroy process")
    return subprocess.run(
        ["molecule", "destroy"],
        cwd="roles/.gitlab-ci")

def run_all_tests():
    molecule_roles = roles('./roles')
    results = []
    for role in molecule_roles:
        role_results = flatten(scenario.test() for scenario in role.scenarios())
        results = results + role_results
    return results

# The main function executes all the molecule tests, destroys the cluster, and deals with failures.
def main():
    results = run_all_tests()

    destroy_successful = destroy_result().returncode == SUCCESS_EXIT_CODE
    if not destroy_successful:
        print("Destroy failed!")

    failed_results = list(result for result in results if result.returncode() != SUCCESS_EXIT_CODE)
    if any(failed_results):
        show_failed_scenarios(failed_results)

    if any(failed_results) or not destroy_successful:
        sys.exit(ERROR_EXIT_CODE)
    else:
        print("Tests completed without errors!")


if __name__ == '__main__':
    main()
